(function() {
    
    var CartApp = angular.module("CartApp", ["ui.router"]);
    
    var MainCtrl = function() {
        var vm = this;
        vm.item = "";
        vm.cart = [];
        vm.addToCart = function() {
            vm.cart.push(vm.item);
            vm.item = "";
        }
    };
    var ProductCtrl = function($stateParams, $state) {
        var vm = this;
        vm.myItem = $stateParams.pid;
        vm.goBack = function() {
            $state.go("main");
        }
    }
    
    var CartConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/views/main.html",
                controller: [MainCtrl],
                controllerAs: "ctrl"
            })
            .state("product", {
                url: "/product/:pid",
                templateUrl: "/views/product.html",
                controller: ["$stateParams", "$state", ProductCtrl],
                controllerAs: "ctrl"
            })
        $urlRouterProvider.otherwise("/main");
    };
    var CartCtrl = function() {
        
    }
    
    CartApp.config(["$stateProvider", "$urlRouterProvider", CartConfig]);
    CartApp.controller("CartCtrl", [CartCtrl]);
})();